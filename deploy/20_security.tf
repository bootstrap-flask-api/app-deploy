##############################################
############ SECURITY GROUPS #################
##############################################

resource "aws_security_group" "allow_http_from_internet" {
  name        = "sg_allow_http"
  description = "Allow http traffic from the internet"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http_from_front_lb" {
  name        = "sg_allow_http_from_public_subnet"
  description = "Allow http traffic from public subnets"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "TCP"
    security_groups = [aws_security_group.allow_http_from_internet.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http_from_private_front_instances" {
  name        = "allow_http_from_private_front_instances"
  description = "Allow http traffic from private front subnets subnet"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "TCP"
    security_groups = [aws_security_group.allow_http_from_front_lb.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http_from_back_lb" {
  name        = "allow_http_from_back_lb"
  description = "Allow http traffic from private front subnets subnet"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "TCP"
    security_groups = [aws_security_group.allow_http_from_private_front_instances.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

##############################################
######### BASTION ADDITIONAL RULES ###########
##############################################

resource "aws_security_group" "allow_ssh_from_internet" {
  name        = "sg_allow_ssh"
  description = "Allow ssh traffic from the internet"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "allow_ssh_from_public_subnet" {
  count             = var.instance_bastion == true ? 1 : 0
  security_group_id = aws_security_group.allow_http_from_front_lb.id

  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.allow_ssh_from_internet.id
}

resource "aws_security_group_rule" "allow_ssh_from_private_front_subnet" {
  count             = var.instance_bastion == true ? 1 : 0
  security_group_id = aws_security_group.allow_http_from_back_lb.id

  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.allow_http_from_front_lb.id
}