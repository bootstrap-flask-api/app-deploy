##############################################
################## NETWORK ###################
##############################################

#### REGION AVAILABILITY ZONES
##############################

data "aws_availability_zones" "availability_zones" {
  state = "available"
}

#### VPC
########

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "${var.project_name}-vpc"
  }
}

#### PUBLIC SUBNETS
###################

#### IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-igw"
  }
}

#### SUBNETS
resource "aws_subnet" "sn_public" {
  count = length(data.aws_availability_zones.availability_zones.names)
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    count.index + var.public_networks_prefix,
  )
  vpc_id                  = aws_vpc.vpc.id
  availability_zone       = data.aws_availability_zones.availability_zones.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project_name}-sn-public-${count.index}"
  }
}

#### ROUTES
resource "aws_route_table" "routetable_public" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-rt-public"
  }
}

resource "aws_route" "route_public" {
  route_table_id         = aws_route_table.routetable_public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "routetable_association_public" {
  count          = length(aws_subnet.sn_public)
  subnet_id      = element(aws_subnet.sn_public.*.id, count.index)
  route_table_id = aws_route_table.routetable_public.id
}

#### PRIVATE SUBNETS CONNECTIVITY
#################################

#### EIP
resource "aws_eip" "eip" {
  count = length(data.aws_availability_zones.availability_zones.names)
  vpc   = true

  tags = {
    Name = "${var.project_name}-eip-front-${count.index}"
  }
}

#### NAT GATEWAY
resource "aws_nat_gateway" "natgw" {
  count         = length(aws_eip.eip.*.id)
  allocation_id = element(aws_eip.eip.*.id, count.index)
  subnet_id     = element(aws_subnet.sn_public.*.id, count.index)

  tags = {
    Name = "${var.project_name}-ngw-${count.index}"
  }
}

#### PRIVATE SUBNETS FRONT
##########################

#### SUBNETS
resource "aws_subnet" "sn_front" {
  count = length(data.aws_availability_zones.availability_zones.names)
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    count.index + var.private_front_networks_prefix,
  )
  vpc_id            = aws_vpc.vpc.id
  availability_zone = element(data.aws_availability_zones.availability_zones.names, count.index)

  tags = {
    Name = "${var.project_name}-sn-private-front-${count.index}"
  }
}

#### ROUTES
resource "aws_route_table" "routetable_front" {
  count  = length(aws_nat_gateway.natgw)
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-rt-front-${count.index}"
  }
}

resource "aws_route" "rt_front" {
  count                  = length(aws_route_table.routetable_front)
  route_table_id         = element(aws_route_table.routetable_front.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
}

resource "aws_route_table_association" "routetable_association_front" {
  count          = length(aws_route.rt_front)
  subnet_id      = element(aws_subnet.sn_front.*.id, count.index)
  route_table_id = element(aws_route_table.routetable_front.*.id, count.index)
}

#### PRIVATE SUBNETS BACK
#########################

#### SUBNETS
resource "aws_subnet" "sn_back" {
  count = length(data.aws_availability_zones.availability_zones.names)
  cidr_block = cidrsubnet(
    aws_vpc.vpc.cidr_block,
    var.netbits,
    count.index + var.private_back_networks_prefix,
  )
  vpc_id            = aws_vpc.vpc.id
  availability_zone = element(data.aws_availability_zones.availability_zones.names, count.index)

  tags = {
    Name = "${var.project_name}-sn-private-back-${count.index}"
  }
}

#### ROUTES
resource "aws_route_table" "routetable_back" {
  count  = length(aws_nat_gateway.natgw)
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-rt-back-${count.index}"
  }
}

resource "aws_route" "rt_back" {
  count                  = length(aws_route_table.routetable_back)
  route_table_id         = element(aws_route_table.routetable_back.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
}

resource "aws_route_table_association" "routetable_association_back" {
  count          = length(aws_route.rt_back)
  subnet_id      = element(aws_subnet.sn_back.*.id, count.index)
  route_table_id = element(aws_route_table.routetable_back.*.id, count.index)
}