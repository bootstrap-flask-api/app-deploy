##############################################
############## FRONT INSTANCES ###############
##############################################

resource "aws_launch_template" "back_launch_template" {
  name                    = "back-instances"
  description             = "back instances template"
  image_id                = data.aws_ami.aws_linux_2.id
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [aws_security_group.allow_http_from_back_lb.id]
  user_data               = base64encode(file("templates/back.tpl"))
  disable_api_termination = false
  key_name                = var.instance_bastion == true ? var.key_name : null

  tags = {
    Name = "${var.project_name}-lt-back"
  }
}

resource "aws_autoscaling_group" "back_instances_autoscaling" {
  depends_on                = [aws_route_table_association.routetable_association_back]
  vpc_zone_identifier       = aws_subnet.sn_back.*.id
  name                      = "${var.project_name}-back-asg"
  target_group_arns         = [aws_lb_target_group.back_target_group.arn]
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  launch_template {
    id = aws_launch_template.back_launch_template.id
  }

  tags = [
    {
      key                 = "Name"
      value               = "${var.project_name}-asg-back"
      propagate_at_launch = true
    }
  ]
  lifecycle {
    create_before_destroy = "true"
  }
}