resource "aws_instance" "bastion" {
  count                       = var.instance_bastion == true ? 1 : 0
  ami                         = data.aws_ami.aws_linux_2.id
  instance_type               = "t2.micro"
  key_name                    = var.instance_bastion == true ? var.key_name : null
  vpc_security_group_ids      = [aws_security_group.allow_ssh_from_internet.id]
  subnet_id                   = aws_subnet.sn_public[0].id
  associate_public_ip_address = true

  tags = {
    Name = "${var.project_name}-bastion"
  }
}