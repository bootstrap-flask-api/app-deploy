output "front_lb_dns" {
  value = "Application url: http://${aws_lb.front_load_balancer.dns_name}"
}