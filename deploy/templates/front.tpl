#!/bin/bash
yum update -y
amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
yum install -y httpd mariadb-server git
git clone https://gitlab.com/bootstrap-flask-api/frontend.git
mv frontend/www/html/* var/www/html/
bash -c 'echo backend_ip = \"${front_lb_dns}\"\; >> var/www/html/js/scripts.js'
bash -c 'echo -e "LoadModule proxy_module modules/mod_proxy.so \nLoadModule proxy_http_module modules/mod_proxy_http.so" >> /etc/httpd/conf/httpd.conf'
bash -c 'echo -e "<VirtualHost *:80> \nProxyPreserveHost On \nProxyPass \"/v1/\" \"http://${back_lb_dns}/v1/\" \nProxyPassReverse \"/v1/\" \"http://${back_lb_dns}/v1/\" \n</VirtualHost>" >> /etc/httpd/conf/httpd.conf'
systemctl start httpd
systemctl enable httpd