#####################
#### GLOBAL VARIABLES
#####################

variable "aws_region" {
  type        = string
  description = "AWS region name to deploy the infrastructure"
  #default     = "" # the aws region that will host the infrastructure
}

variable "aws_profile" {
  type        = string
  description = "AWS profile that will be used to deploy the infrastructure"
  #default     = "" # the IAM user that will perform the deployment
}

variable "project_name" {
  type        = string
  description = "Project name used to tag resources"
  default     = "flask-api"
}

#######################
#### OPTIONAL - BASTION
#######################

variable "instance_bastion" {
  type        = bool
  description = "add an instance in a public subnet to access both private_front and private_back subnets"
  default     = false # if true, key_name is mandatory
}

variable "key_name" {
  type        = string
  description = "Key name as registered in AWS used to access all instances"
  default     = null
}

######################
#### NETWORK VARIABLES
######################

variable "vpc_cidr" {
  type        = string
  description = "CIDR of the VPC"
  default     = "10.0.0.0/16"
}

variable "netbits" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "8"
}

variable "public_networks_prefix" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "10"
}

variable "private_front_networks_prefix" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "20"
}

variable "private_back_networks_prefix" {
  type        = string
  description = "Used to calculate subnets CIDR blocks"
  default     = "30"
}

###################################
#### LOAD BALANCERS ACCOUNT MAPPING
###################################

variable "elb_accounts_id" {
  type = "map"
  default = {
    "us-east-1"      = "127311923021"
    "us-east-2"      = "033677994240"
    "us-west-1"      = "027434742980"
    "us-west-2"      = "797873946194"
    "ca-central-1"   = "985666609251"
    "eu-central-1"   = "054676820928"
    "eu-west-1"      = "156460612806"
    "eu-west-2"      = "652711504416"
    "eu-west-3"      = "009996457667"
    "eu-north-1"     = "897822967062"
    "ap-east-1"      = "754344448648"
    "ap-northeast-1" = "582318560864"
    "ap-northeast-2" = "600734575887"
    "ap-northeast-3" = "383597477331"
    "ap-southeast-1" = "114774131450"
    "ap-southeast-2" = "783225319266"
    "ap-south-1"     = "718504428378"
    "me-south-1"     = "076674570225"
    "sa-east-1"      = "507241528517"
  }
}