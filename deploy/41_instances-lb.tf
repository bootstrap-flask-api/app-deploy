##############################################
############## LOAD BALANCERS ################
##############################################

#### S3 bucket for logs
#######################

resource "aws_s3_bucket" "load_balancers_logs" {
  bucket        = "${var.project_name}-alb-logs"
  acl           = "private"
  policy        = templatefile("policies/bucket_alb_logs.json", { elb_account_id = var.elb_accounts_id[var.aws_region], bucket_name = "${var.project_name}-alb-logs" })
  force_destroy = true

  tags = {
    Name = "${var.project_name}-logs-alb"
  }
}

#### ALB from internet to front instances
#########################################

resource "aws_lb" "front_load_balancer" {
  name                             = "${var.project_name}-lb-front"
  internal                         = false
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.allow_http_from_internet.id]
  subnets                          = aws_subnet.sn_public.*.id
  enable_cross_zone_load_balancing = true
  ip_address_type                  = "ipv4"
  enable_deletion_protection       = false

  access_logs {
    bucket  = aws_s3_bucket.load_balancers_logs.bucket
    prefix  = "front-elb"
    enabled = true
  }

  tags = {
    Name = "${var.project_name}-lb-front"
  }
}

resource "aws_lb_target_group" "front_target_group" {
  name        = "${var.project_name}-tg-front"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc.id
  target_type = "instance"

  health_check {
    enabled             = true
    interval            = 5
    path                = "/index.html"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 2
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = {
    Name = "${var.project_name}-tg-front"
  }
}

resource "aws_lb_listener" "front_alb_listener" {
  load_balancer_arn = aws_lb.front_load_balancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_target_group.arn
  }
}

#### ALB from front instances to back instances
###############################################
resource "aws_lb" "back_load_balancer" {
  name                             = "${var.project_name}-lb-back"
  internal                         = true
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.allow_http_from_private_front_instances.id]
  subnets                          = aws_subnet.sn_back.*.id
  enable_cross_zone_load_balancing = true
  ip_address_type                  = "ipv4"
  enable_deletion_protection       = false

  access_logs {
    bucket  = aws_s3_bucket.load_balancers_logs.bucket
    prefix  = "back-elb"
    enabled = true
  }

  tags = {
    Name = "${var.project_name}-lb-back"
  }
}

resource "aws_lb_target_group" "back_target_group" {
  name        = "${var.project_name}-tg-back"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc.id
  target_type = "instance"

  health_check {
    enabled             = true
    interval            = 5
    path                = "/documentation"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 2
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = {
    Name = "${var.project_name}-tg-back"
  }
}

resource "aws_lb_listener" "back_alb_listener" {
  load_balancer_arn = aws_lb.back_load_balancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.back_target_group.arn
  }
}
