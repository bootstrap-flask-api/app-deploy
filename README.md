# Flask API - deploy

## What it does
Automatically deploys the full application on AWS using Terraform.
- [backend details](https://gitlab.com/bootstrap-flask-api/backend)
- [frontend details](https://gitlab.com/bootstrap-flask-api/frontend)

## Architecture
![app architecture](/img/architecture.png)

## How to run
- Prerequisites
  - an AWS IAM user with at least the following policies:
    - AmazonVPCFullAccess
    - AmazonEC2FullAccess
    - AmazonS3FullAccess
  - [install terraform](https://learn.hashicorp.com/terraform/getting-started/install)
- open your terminal in app-deploy/deploy/ then:
```
$ terraform init
$ terraform apply
```
- to stop being asked for a region and profile, uncomment `default` in 00_variables.tf
and set your AWS region and profile name:
```
variable "aws_region" {
  type        = string
  description = "AWS region name to deploy the infrastructure"
  #default     = ""     # the aws region that will host the infrastructure
}

variable "aws_profile" {
  type        = string
  description = "AWS profile that will be used to deploy the infrastructure"
  #default     = ""     # the IAM user that will perform the deployment
}
```
- if you want to access instances, change the default value of the variable `instance_bastion` to `true`
and replace the `key_name` variable default value from `null` to any ssh key you can access registered 
in your AWS account.
```
#######################
#### OPTIONAL - BASTION
#######################

variable "instance_bastion" {
  type        = bool
  description = "add an instance in a public subnet to access both private_front and private_back subnets"
  default     = false # if true, key_name is mandatory
}

variable "key_name" {
  type        = string
  description = "Key stored in AWS for ssh access"
  default     = null # change for your ssh key to access the instance, if needed
}
```
- when the infrastructure will be created, access to any ec2 instance with:
````
$ eval `ssh-agent` # activate ssh-agent
$ ssh-add PATH-TO-YOUR-SSH-PRIVATE-KEY
$ ssh -A ec2-user@BASTION-INSTANCE-IP
BASTION-INSTANCE-IP$ ssh ec2-user@FRONT|BACK-INSTANCE-IP 
````
- clean after your tests
```
$ terraform destroy
```

## TODO:
- add EFS to store the website
- add EFS to store the backend app
- connect both EFS to instances in templates
- add and configure database
- add and configure S3 bucket + update front
- add and configure SQS queue + update front